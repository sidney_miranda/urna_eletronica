window.addEventListener('load', removeFoto());
const candidates = [
    {
        nome: 'Voto em Branco',
        pontos: 0,
        numero: '**',
        foto: './images/default.png',
        partido: 'N/A'
    },
    {
        nome: 'Voto Nulo',
        pontos: 0,
        numero: null,
        foto: './images/default.png',
        partido: 'N/A'
    },
    {
        nome: 'Homem Aranha',
        pontos: 0,
        numero: '10',
        foto: './images/homem_aranha.png',
        partido: 'Marvel'
    },
    {
        nome: 'Capitão América',
        pontos: 0,
        numero: '11',
        foto: './images/cap_america.png',
        partido: 'Marvel'
    },
    {
        nome: 'Homem de Ferro',
        pontos: 0,
        numero: '12',
        foto: './images/homem_ferro.png',
        partido: 'Marvel'
    },
    {
        nome: 'Thor',
        pontos: 0,
        numero: '13',
        foto: './images/thor.png',
        partido: 'Marvel'
    },
    {
        nome: 'Hulk',
        pontos: 0,
        numero: '14',
        foto: './images/hulk.png',
        partido: 'Marvel'
    },
    {
        nome: 'Falcon Hero',
        pontos: 0,
        numero: '15',
        foto: './images/falcon_hero.png',
        partido: 'Marvel'
    },
    {
        nome: 'Loki',
        pontos: 0,
        numero: '16',
        foto: './images/loki.png',
        partido: 'Marvel'
    },
    {
        nome: 'Arqueiro',
        pontos: 0,
        numero: '17',
        foto: './images/hawkeye.png',
        partido: 'Marvel'
    },
    {
        nome: 'Batman',
        pontos: 0,
        numero: '90',
        foto: './images/batman.png',
        partido: 'DC'
    },
    {
        nome: 'Raven',
        pontos: 0,
        numero: '91',
        foto: './images/raven.png',
        partido: 'DC'
    },
    {
        nome: 'Flash',
        pontos: 0,
        numero: '92',
        foto: './images/flash.png',
        partido: 'DC'
    },
    {
        nome: 'Cyborg',
        pontos: 0,
        numero: '93',
        foto: './images/cyborg.png',
        partido: 'DC'
    },
    {
        nome: 'Super-Man',
        pontos: 0,
        numero: '94',
        foto: './images/super_man.png',
        partido: 'DC'
    },
    {
        nome: 'Mulher-Maravilha',
        pontos: 0,
        numero: '95',
        foto: './images/wonder_woman.png',
        partido: 'DC'
    },
];

var selecionado;
var eleitores = [];

function removeFoto() {
    document.querySelector('#div-foto').setAttribute('style', 'display:none');
}

function clickButton(e) {
    let number = e;
    addNumero(number);
}

function addNumero(n) {
    let num1 = document.querySelector("#n1");
    let num2 = document.querySelector("#n2");
    if (num1.value == "")
        num1.value = n;
    else if (num2.value == "") {
        num2.value = n;
        selecionado = String(num1.value) + String(num2.value);
        atualizaInfo(selecionado);
    }
}

function atualizaInfo(candidate) {
    let nome = document.querySelector('#nome');
    let partido = document.querySelector('#partido');

    let selecionado = candidates.filter(c => c.numero == candidate);
    if (selecionado.length == 0) selecionado = candidates.filter(c => c.numero == null);

    document.querySelector('#foto').setAttribute('src', selecionado.map(i => i.foto));
    document.querySelector('#div-foto').removeAttribute('style', 'display:none');

    nome.value = selecionado.map(c => c.nome.toUpperCase());
    nome.setAttribute('class', 'badge badge-dark p-1');

    partido.value = selecionado.map(c => c.partido.toUpperCase());
    partido.setAttribute('class', 'badge badge-dark p-1');
}

function limpar() {
    document.querySelector("#n1").value = "";
    document.querySelector("#n2").value = "";
    document.querySelector("#candidatos").value = "";
    document.querySelector('#nome').value = "";
    document.querySelector('#partido').value = "";
    document.querySelector('#rg').value = "";
    removeFoto();
}

document.querySelector('#btOne').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btTwo').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btThree').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btFour').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btFive').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btSix').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btSeven').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btEight').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btNine').addEventListener('click', (e) => clickButton(e.target.value))
document.querySelector('#btZero').addEventListener('click', (e) => clickButton(e.target.value))

document.querySelector('#candidatos').addEventListener('change', selection, false);

document.querySelector('#confirmar').addEventListener('click', confirmar);
document.querySelector('#vtBranco').addEventListener('click', votoBranco);
document.querySelector('#limpar').addEventListener('click', limpar);

function confirmar() {
    let n1 = document.querySelector("#n1").value;
    let n2 = document.querySelector("#n2").value;
    let numero = String(n1) + String(n2);

    let rg = document.querySelector('#rg').value;

    if (n1 != "" && n2 != "") {

        eleitores.push({
            eleitor: rg,
            candidato: numero,
            registro: new Date().toLocaleString()
        });
        console.log(eleitores);
        switch (numero) {
            case '**':
            case '11':
            case '12':
            case '13':
            case '14':
            case '15':
            case '16':
            case '17':
            case '90':
            case '91':
            case '92':
            case '93':
            case '94':
            case '95':
                candidates
                    .map(c => (c.numero == selecionado) ? ++c.pontos : c.pontos);
                break;
            default:
                candidates
                    .map(c => (c.nome == 'Voto Nulo') ? ++c.pontos : c.pontos);
                break;
        }
        limpar();
    } else {
        alert("Verifique os campos obrigatórios!");
    }
    atualizaTabela();
}

function votoBranco() {
    let n1 = document.querySelector("#n1").value = '*';
    let n2 = document.querySelector("#n2").value = '*';
    selecionado = String(n1) + String(n2);
    atualizaInfo(selecionado);
}

function selection() {
    var select = document.querySelector('#candidatos');
    const value = select.options[select.selectedIndex].value;
    let num1 = document.querySelector("#n1");
    let num2 = document.querySelector("#n2");

    num1.value = value.substr(0, 1);
    num2.value = value.substr(1, 2);
    selecionado = String(num1) + String(num2);
    atualizaInfo(selecionado);
}

function atualizaTabela() {
    let tbody = document.querySelector('#tbody');
    var total = candidates.map(c => c.pontos).reduce((acc, cur) => acc + cur);
    candidates.sort((a, b) => b.pontos - a.pontos);
    tbody.innerHTML = "";

    candidates.map(c => {
        var row = document.createElement("tr");
        let cell1 = document.createElement("td");
        let cell2 = document.createElement("td");
        let cell3 = document.createElement("td");

        tbody.appendChild(row);

        row.appendChild(cell1);
        cell1.append(document.createTextNode(c.nome));
        row.appendChild(cell2);
        cell2.append(document.createTextNode((c.partido) ? c.partido : '-'));
        row.appendChild(cell3);
        cell3.append(document.createTextNode((c.pontos / total * 100).toFixed(1) + '%'));

        tbody.appendChild(row);
    });
}